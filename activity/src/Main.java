import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;
        Scanner myObj = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        System.out.println("First Name: ");
        firstName = myObj.nextLine();
        System.out.println("Last Name: ");
        lastName = myObj.nextLine();
        System.out.println("First Subject Grade: ");
        firstSubject = myObj.nextDouble();
        System.out.println("Second Subject Grade: ");
        secondSubject = myObj.nextDouble();
        System.out.println("Third Subject Grade: ");
        thirdSubject = myObj.nextDouble();

        double averageGrade = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + df.format(averageGrade));
    }
}